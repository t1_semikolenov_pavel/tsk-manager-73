package ru.t1.semikolenov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    RoleType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
