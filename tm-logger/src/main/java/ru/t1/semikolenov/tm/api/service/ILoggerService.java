package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull String message);

}
