<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Project List</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap" align="left">Name</th>
        <th width="100%" align="left">Description</th>
        <th width="150" align="center" nowrap="nowrap">Status</th>
        <th width="100" align="center" nowrap="nowrap">Date Begin</th>
        <th width="100" align="center" nowrap="nowrap">Date End</th>
        <th width="100" align="center" nowrap="nowrap">Edit</th>
        <th width="100" align="center" nowrap="nowrap">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center">
                <c:out value="${project.status.displayName}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateBegin}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateEnd}"/>
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}/">Edit</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}/">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
