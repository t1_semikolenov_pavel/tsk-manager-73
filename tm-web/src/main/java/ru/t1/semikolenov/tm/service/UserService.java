package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.semikolenov.tm.enumerated.RoleType;
import ru.t1.semikolenov.tm.exception.field.EmptyLoginException;
import ru.t1.semikolenov.tm.exception.field.EmptyPasswordException;
import ru.t1.semikolenov.tm.model.Role;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Modifying
    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        @NotNull final Role role = roleType == null ? new Role(user, RoleType.USUAL) : new Role(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @NotNull
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findFirstByLogin(login);
    }

}
