package ru.t1.semikolenov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_begin")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    public AbstractUserOwnedModel(
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin
    ) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
    }

    public AbstractUserOwnedModel(
            @NotNull final String name
    ) {
        this.name = name;
    }

}
