package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}