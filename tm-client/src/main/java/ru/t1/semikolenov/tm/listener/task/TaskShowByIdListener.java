package ru.t1.semikolenov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.dto.request.TaskShowByIdRequest;
import ru.t1.semikolenov.tm.dto.response.TaskShowByIdResponse;
import ru.t1.semikolenov.tm.event.ConsoleEvent;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @NotNull final TaskShowByIdResponse response = getTaskEndpoint().showTaskById(request);
        @Nullable final TaskDTO task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}