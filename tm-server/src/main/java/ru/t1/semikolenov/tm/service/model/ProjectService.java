package ru.t1.semikolenov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.service.model.IProjectService;
import ru.t1.semikolenov.tm.exception.field.EmptyDescriptionException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.repository.model.ProjectRepository;
import ru.t1.semikolenov.tm.repository.model.UserRepository;

import java.util.Date;

@Service
public class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    protected ProjectRepository getRepository() {
        return repository;
    }

    @NotNull
    private UserRepository getUserRepository() {
        return userRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findById(userId).orElse(null));
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final ProjectRepository repository = getRepository();
        repository.save(project);
        return project;
    }

}
