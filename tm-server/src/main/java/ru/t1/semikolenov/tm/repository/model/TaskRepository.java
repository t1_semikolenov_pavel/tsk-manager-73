package ru.t1.semikolenov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Transactional
    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
