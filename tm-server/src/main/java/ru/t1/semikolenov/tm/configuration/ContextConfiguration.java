package ru.t1.semikolenov.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.semikolenov.tm.api.service.IDatabaseProperty;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.semikolenov.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.t1.semikolenov.tm.repository")
public class ContextConfiguration {

    @NotNull
    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUser());
        dataSource.setPassword(databaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        @Nullable final EntityManagerFactory factory = entityManagerFactory.getObject();
        transactionManager.setEntityManagerFactory(factory);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.semikolenov.tm.model", "ru.t1.semikolenov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getCacheProviderConfig());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getCacheRegionFactory());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
